var game = document.querySelector('.js-game');
var grid = [];

var GAME_ROW_LENGTH = 10;
var GAME_CELLS_LENGTH = Math.pow(GAME_ROW_LENGTH, 2);

// Create grid
for(var i = 0; i < GAME_CELLS_LENGTH; i++) {
    var cell = document.createElement('div');
    cell.className = 'cell';
    cell.style.width = GAME_ROW_LENGTH + '%';
    cell.innerHTML = i;
    game.append(cell);

    grid.push(cell);
}

// Food
var currentFoodPosition = 0;
var counterFood = -1;
var foodTimeout;
function createFood() {
    grid[currentFoodPosition].classList.remove('food');

    currentFoodPosition = Math.floor(Math.random() * GAME_CELLS_LENGTH);
    
    if(grid[currentFoodPosition].classList.contains('snake')) {
        createFood();
    } else {
        grid[currentFoodPosition].classList.add('food');

        clearTimeout(foodTimeout);
        foodTimeout = setTimeout(function() {
            createFood();
        }, 5000);
    }
}

var LEFT_DIR = 'KeyA';
var RIGHT_DIR = 'KeyD';
var UP_DIR = 'KeyW';
var BOTTOM_DIR = 'KeyS';

var currentDirection = RIGHT_DIR;

var canChangeDirection = true
function changeDirection(newDirection) {

    if(currentDirection === newDirection || !canChangeDirection) return;

    // if(newDirection === LEFT_DIR && currentDirection !== RIGHT_DIR) {
    //     currentDirection = newDirection
    // } else if(newDirection === RIGHT_DIR && currentDirection !== LEFT_DIR) {
    //     currentDirection = newDirection
    // } else if(newDirection === UP_DIR && currentDirection !== BOTTOM_DIR) {
    //     currentDirection = newDirection
    // } else if(newDirection === BOTTOM_DIR && currentDirection !== UP_DIR) {
    //     currentDirection = newDirection
    // }
    
    if(
        (newDirection === LEFT_DIR && currentDirection !== RIGHT_DIR) ||
        (newDirection === RIGHT_DIR && currentDirection !== LEFT_DIR) ||
        (newDirection === UP_DIR && currentDirection !== BOTTOM_DIR) ||
        (newDirection === BOTTOM_DIR && currentDirection !== UP_DIR)
    ) {
        currentDirection = newDirection
        canChangeDirection = false
    }

}

var currentSnakeHeadPosition = GAME_CELLS_LENGTH / 2;
var isStart = true

var SPEED = 100;
var snakeLength = SPEED * 4;

function moveSnake() {

    grid[currentSnakeHeadPosition].classList.remove('head')

    if(!isStart) {
        switch(currentDirection) {
            case RIGHT_DIR:
                currentSnakeHeadPosition += 1
                if(currentSnakeHeadPosition % GAME_ROW_LENGTH === 0 ||
                    currentSnakeHeadPosition > GAME_CELLS_LENGTH - 1) {
                    currentSnakeHeadPosition -= GAME_ROW_LENGTH
                }
                break;
            case LEFT_DIR:
                currentSnakeHeadPosition -= 1
                if(currentSnakeHeadPosition % GAME_ROW_LENGTH === GAME_ROW_LENGTH - 1 ||
                    currentSnakeHeadPosition < 0) {
                    currentSnakeHeadPosition += GAME_ROW_LENGTH
                }
                break;
            case UP_DIR:
                currentSnakeHeadPosition -= GAME_ROW_LENGTH
                if(currentSnakeHeadPosition < 0) {
                    currentSnakeHeadPosition += GAME_CELLS_LENGTH
                }
                break;
            case BOTTOM_DIR:
                currentSnakeHeadPosition += GAME_ROW_LENGTH
                if(currentSnakeHeadPosition > GAME_CELLS_LENGTH - 1) {
                    currentSnakeHeadPosition -= GAME_CELLS_LENGTH
                }
                break;
        }
    }

    isStart = false;

    var nextSnakeHeadCell = grid[currentSnakeHeadPosition]

    if(nextSnakeHeadCell.classList.contains('snake')) {
        clearInterval(gameInterval);
        alert('Game Over!');
        window.location.reload();
    }

    nextSnakeHeadCell.classList.add('snake');
    nextSnakeHeadCell.classList.add('head');

    setTimeout(function() {
        nextSnakeHeadCell.classList.remove('snake');
    }, snakeLength);
    
    if(nextSnakeHeadCell.classList.contains('food')) {
        snakeLength += SPEED

        createFood();
        
        // увеличиваем счетчик
        counterFood += 1;
        // прописать в span на странице
        console.log(counterFood)
    }

    canChangeDirection = true
}

// Events
document.addEventListener('keypress', function(event) {
    changeDirection(event.code);
});



// Start Game
createFood();

var gameInterval = setInterval(function() {
    moveSnake();
}, SPEED)